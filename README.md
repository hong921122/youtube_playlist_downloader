# README #

Youtube Playlist Downloader

### Youtube Playlist Downloader ###

* 기존에 유튜브 동영상 다운로더는 단일 동영상만 가능
* 플레이 리스트 링크를 넘기면 해당 플레이리스트 다운로드

### Usage ###
* 파이썬코드 받아서 argument로 url을 넘기면됨
* 비공개 플레이리스트의 경우에는 다운로드 아직은 안됨 (추가 예정)

### Requirements ###
* FFMPEG binary (https://www.ffmpeg.org/)